package main

import (
	"fmt"

	dir "com.mohamedms.golang/goland-sample-dep/dir"
)

func main() {
	fmt.Println("Main from go land sample dep")
	dir.ShowSample()
}
