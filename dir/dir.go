package dir

import (
	"fmt"

	dir2 "com.mohamedms.golang/goland-sample-dep/dir2"
)

func ShowSample() {
	fmt.Println("sample from dir package")
	dir2.FromDirectory2()
}
